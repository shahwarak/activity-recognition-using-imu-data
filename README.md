
# Physical Activity Recogntion using Inertial Measurement Units (IMUs)

  

## Description

  

This work involves development and evaluation of a bi-layer classification model to detect physical activities from a multivariate time series dataset namely PAMAP2 [[1]](#1). The dataset contains data acquired by recording 9 subject wearing 3 inertial measurement units (on their hand, chest and ankle) and a heart rate monitor, and performing 18 different activities. The first layer of the bi-layer classification aims to detect the intensity of the activity performed i.e. rest, low, medium and high, and the second layer uses the results of the first layer to perform the final classification. For the second classification layer, the time series was divided to smaller time windows of fixed intervals, from which various statistical, energy, frequency and correlation-based features were calculated and classifiers using these features were tested. Each classifier was tested using two evaluation methods: within-subject cross validation and cross-subject cross validation. Either Random Forests or K- Nearest Neighbor for intensity classification, followed by Support vector machines for activity classification worked best for within-subject evaluation, with a classification accuracy of 98.7%, while the best results for between subject was observed when both the layers were Random forests, with an accuracy of 75.93%.

  
  ## Dataset
  Raw dataset used:
https://archive.ics.uci.edu/ml/machine-learning-databases/00231/

## Running the code

To run the code add all 6 following .py files to an environment:
	1. preprocess_1.py 
	2. main.py
	3. main_ts_l2.py
	4. time_series_feat.py
	5. ts_prep.py
	6. ts_eval.py

For intensity based classification:

Change the classifier by giving the function get_model the right argument in line 191 of main.py
Change the pathname to the desired path in line 282 of main.py
Optional: Change the pathname to the desired path in line 180 of main.py if the data frame from preprocess_1.py is saved

Activity based classification:

Change the file path to the .csv file saved at the end of running main.py in line 14 of main_ts_l2.py
Set the time series overlap % in line 23 of main_ts_l2.py

For the heart rate regression model:
In the main.py file uncomment indicated parts in the following functions:
* main() 
* within_subject()
* between_subject()
									 
Change the last argument of evaluation() to 1 on lines 96 and 139 in main.py

  

## References

<a  id="1">[1]</a>  Attila Reiss and Didier Stricker. Creating and benchmarking a new dataset for physical activity monitoring. In Proceedings of the 5th International Conference on Pervasive Technologies Related to Assistive Environments, pages 1–8, 2012.
